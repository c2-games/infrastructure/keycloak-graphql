import uvicorn

from fastapi import FastAPI

from keycloak_graphql import v1, make_gql_app
from keycloak_graphql.config import get_config


config = get_config()
gqlApp = make_gql_app(
    debug=config.debug,
    introspection=config.introspection,
    playground=config.playground
)


app = FastAPI()
app.include_router(v1.router, prefix="/v1")
app.mount("/", gqlApp)

if __name__ == "__main__":
    uvicorn.run(app, host=config.host, port=config.port)
