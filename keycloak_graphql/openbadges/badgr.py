"""OpenBadges Badgr Provider"""
import json
from typing import Optional, List, Dict, TypedDict
from datetime import datetime, timedelta
from urllib.parse import urljoin
import requests
from gql.transport.exceptions import TransportQueryError
from keycloak.exceptions import KeycloakGetError

from pydantic import BaseModel, PrivateAttr, validator
from requests import HTTPError

from ..cache_manager import CacheManager

from . import OpenBadgesAssertion, OpenBadgesIssuer, OpenBadgesBadgeClass, OpenBadgesAssertionEvidence
from ..gql_bridge import GqlBridge
from ..gql_bridge.types import AwardAssertion, User
from ..keycloak_bridge import KeycloakBridge


class BadgrValidationError(Exception):
    """Custom Exception class that will have it's message forwarded to the client."""


class MutationResult(BaseModel):
    success: bool
    modified: bool
    error: Optional[str] = None


class AssertionExtra(TypedDict):
    narrative: str
    allow_duplicate: bool


class BadgrProvider(BaseModel):
    kcb: KeycloakBridge
    gqlb: GqlBridge
    username: str
    password: str
    issuer_id: str = "X78X-MX9S6qmwqpqg7LbBw"
    api_root_public: str = "https://api.badgr.io/public/"
    api_root_authenticated: str = "https://api.badgr.io/v2/"
    cache: CacheManager
    debug: bool = False

    # badge names
    competitor_2025: str
    competitor_2024: str
    most_valuable: str
    most_improved: str
    regional_champion: str
    invitational_champion: str
    invitational_competitor: str

    _token: str | None = PrivateAttr(None)
    _refresh_token: str | None = PrivateAttr(None)
    _expiration: datetime | None = PrivateAttr(None)

    class Config:
        arbitrary_types_allowed = True

    @validator('api_root_public', 'api_root_authenticated')
    @classmethod
    def url_end_in_slash(cls, v: str) -> str:
        """Ensure the urls end in a slash so urljoin works expectedly"""
        if not v.endswith("/"):
            return v + "/"
        return v

    def _handle_auth_request(self, data: Dict[str, str]):
        # Remove the slash from the url only in this case
        tmp = self.api_root_authenticated
        if tmp.endswith("/"):
            tmp = tmp[:-1]
        endpoint = urljoin(tmp, "o/token")
        response = requests.post(endpoint, data=data, timeout=10)

        response.raise_for_status()
        data = response.json()
        if "error" in data:
            additional_info = data.get("error_description", "")
            msg = data["error"] + ": " + additional_info if additional_info else data["error"]
            print(f"error getting access token: {msg}")
            raise RuntimeError(msg)
        self._token = data["access_token"]
        self._refresh_token = data["refresh_token"]
        self._expiration = datetime.utcnow() + timedelta(seconds=float(data["expires_in"]))
        if self.debug:
            print(f"Token expires at {self._expiration.isoformat()}")

    def _initial_token(self):
        self._handle_auth_request({
            "username": self.username,
            "password": self.password,
        })

    def _token_refresh(self):
        # if the refresh token isn't set just reauthenticate with initial creds
        if not self._refresh_token:
            self._initial_token()
            return

        self._handle_auth_request({
            "grant_type": "refresh_token",
            "refresh_token": self._refresh_token,
        })

    def get_token(self) -> str:
        """Get the auth token for the Badgr API"""
        # If internals aren't initialized, init them
        if self._token is None or self._expiration is None:
            self._initial_token()
            if self._token is None:
                # It is unlikely that this is ever hit
                raise RuntimeError("Failed to get token")
            return self._token
        # If the token is expired, refresh it
        if self._expiration < datetime.utcnow():
            self._token_refresh()
            return self._token
        # Not expired, token initialized
        return self._token

    def _get_headers(self) -> dict:
        return {"Authorization": f"Bearer {self.get_token()}"}

    def _generate_assertion_extra(self, assertion: AwardAssertion) -> Optional[AssertionExtra]:
        event_text = f'{assertion.event.event_group.name} event {assertion.event.name}'
        if assertion.badge.name == self.competitor_2024:
            return {
                'narrative': f'Competed in the {event_text} with a team of'
                             f' {assertion.team.team_size} from {assertion.team.institution.name}.',
                'allow_duplicate': False
            }
        elif assertion.badge.name == self.competitor_2025:
            return {
                'narrative': f'Competed in the {event_text} with a team of'
                             f' {assertion.team.team_size} from {assertion.team.institution.name}.',
                'allow_duplicate': False
            }
        elif assertion.badge.name in (self.most_valuable, self.most_improved):
            return {
                'narrative': f"Voted {assertion.badge.name} by their team in the {event_text}.",
                'allow_duplicate': True
            }
        elif assertion.badge.name == self.regional_champion:
            return {
                'narrative': f'Won the {event_text} with a team of '
                             f' {assertion.team.team_size} from {assertion.team.institution.name}.',
                'allow_duplicate': True
            }
        elif assertion.badge.name == self.invitational_champion:
            return {
                'narrative': f'Won the {assertion.event.event_group.name} with a team of '
                             f' {assertion.team.team_size} from {assertion.team.institution.name}.',
                'allow_duplicate': True
            }
        elif assertion.badge.name == self.invitational_competitor:
            return {
                'narrative': f'Competed in the {assertion.event.event_group.name} with a team of '
                             f' {assertion.team.team_size} from {assertion.team.institution.name}.',
                'allow_duplicate': True
            }

        return None

    async def assert_badgr_2024(self, assertion_id: str) -> MutationResult:
        """
        Assert a badge from the 2024 NCAE season in Badgr for the specified internal assertion ID.
        :param assertion_id: Internal hasura `awards_assertions.id` field
        :return: True if a new badgr assertion was created, false if the assertion ID already exists
        """
        return await self.assert_badgr_202x(assertion_id, "2024")

    async def assert_badgr_2025(self, assertion_id: str) -> MutationResult:
        """
        Assert a badge from the 2025 NCAE season in Badgr for the specified internal assertion ID.
        :param assertion_id: Internal hasura `awards_assertions.id` field
        :return: True if a new badgr assertion was created, false if the assertion ID already exists
        """
        return await self.assert_badgr_202x(assertion_id, "2025")

    async def assert_badgr_202x(self, assertion_id: str, year: str) -> MutationResult:
        """
        Assert a badge from the 2024 / 2025 (possibly beyond) season in Badgr for the specified internal assertion ID.
        :param assertion_id: Internal hasura `awards_assertions.id` field
        :param year: Year to use in generating the assertion information
        :return: True if a new badgr assertion was created, false if the assertion ID already exists
        """
        # Ignoring because it doesn't make sense to do less return statements
        # pylint: disable=too-many-return-statements
        try:
            assertion = await self.gqlb.get_award_assertion(assertion_id)
        except TransportQueryError as e:
            if isinstance(e.errors, list) and len(e.errors) and "message" in e.errors[0]:
                return MutationResult(success=False, modified=False, error=e.errors[0]['message'])
            return MutationResult(success=False, modified=False, error="Failed to get assertion")
        except KeycloakGetError:
            return MutationResult(success=False, modified=False, error="Failed to retrieve user data")
        except RuntimeError as e:
            return MutationResult(success=False, modified=False, error=str(e))

        if not assertion:
            return MutationResult(success=False, modified=False, error="Assertion not found")

        if assertion.badgr_assertion_id:
            return MutationResult(success=True, modified=False)

        issued_on = datetime.fromisoformat(assertion.event.start).isoformat() if assertion.event.start else None
        evidence = [
            OpenBadgesAssertionEvidence(url=f"https://ncaecybergames.org/results/{year}/teams/{assertion.team_id}")
        ]

        extra = self._generate_assertion_extra(assertion)
        if not extra:
            return MutationResult(success=False, modified=False, error="Badge name not recognized")

        if not assertion.badge.badgr_badge_id:
            print('Failed to assert Badge in Badgr: Badgr Badge ID is not set')
            return MutationResult(success=False, modified=False, error="Badgr Badge ID is not set")

        try:
            badge_assertion = await self._assert_badge_in_badgr(
                assertion.badge.badgr_badge_id,
                assertion.user,
                issued_on,
                evidence=evidence,
                **extra
            )
        except BadgrValidationError as e:
            print('FAILED TO ASSERT BADGE IN BADGR', e)
            return MutationResult(success=False, modified=False, error=str(e))
        except Exception as e:
            print('FAILED TO ASSERT BADGE IN BADGR', e)
            return MutationResult(success=False, modified=False, error="Failed to assert badge in Badgr")

        try:
            await self.gqlb.update_award_assertion(assertion_id, badgr_assertion_id=badge_assertion.get('entityId'))
        except Exception as e:
            print('FAILED TO UPDATE BADGE ASSERTION', e)
            return MutationResult(success=False, modified=True, error="Failed to update assertion with badge ID")

        return MutationResult(success=True, modified=True)

    async def _assert_badge_in_badgr(
            self,
            badge_id: str,
            user: User,
            issue_on: str | None = None,
            narrative: str | None = None,
            evidence: List[OpenBadgesAssertionEvidence] | None = None,
            allow_duplicate=False
    ) -> Dict:
        # get badge to ensure it exists
        badge = self.get_badge_by_id(badge_id)
        body = {
            "issuer": self.issuer_id,
            "badgeclass": badge.id,
            "recipient": {
                "identity": user.email,
                "type": "email"
            },
            "issuedOn": issue_on,
            "allowDuplicateAwards": allow_duplicate,
            "extensions": {
                # display user's first name in the badge assertion
                "extensions:recipientProfile": {
                    "name": user.first_name,
                    "@context": "https://openbadgespec.org/extensions/recipientProfile/context.json",
                    "type": ["Extension", "extensions:RecipientProfile"]
                }
            }
        }
        if self.debug:
            print(json.dumps(body, indent=2))
        if narrative:
            body["narrative"] = narrative
        if evidence:
            body["evidence"] = [e.dict() for e in evidence]
        endpoint = urljoin(self.api_root_authenticated, f"issuers/{self.issuer_id}/assertions")
        response = requests.post(endpoint, timeout=10, json=body, headers=self._get_headers())

        try:
            data = response.json()
        except Exception:
            data = response.text

        try:
            response.raise_for_status()
        except HTTPError as e:
            print(f"error asserting badge: {data}")
            if isinstance(data, dict) and "validationErrors" in data:
                # handle cases like "previous award already exists"
                raise BadgrValidationError(
                    'failed to issue badge assertion: ' + '; '.join(data["validationErrors"])
                ) from e
            raise

        if "status" in data and not data['status'].get('success'):
            msg = data['status'].get("description", "unknown error")
            print(f"error issuing badge: {msg}")
            if self.debug:
                print(f"badgr api: {response.text}")
            raise Exception('failed to issue badge assertion')  # pylint: disable=broad-exception-raised

        result = {}
        if data.get('result'):
            result = data['result']
            if isinstance(result, list):
                result = result[0]

        return result

    def get_assertion_by_id(self, assertion_id: str) -> OpenBadgesAssertion:
        """Get an assertion from the Badgr API."""
        # Get from cache if exists
        ret = self.cache.get(f"get_assertion_by_id:{assertion_id}")
        if isinstance(ret, OpenBadgesAssertion):
            return ret
        elif isinstance(ret, dict):
            return OpenBadgesAssertion(**ret)
        # Else get the info from the remote
        endpoint = urljoin(self.api_root_public, f"assertions/{assertion_id}")
        response = requests.get(endpoint, timeout=10)
        response.raise_for_status()
        resp = response.json()
        if "error" in resp:
            additional_info = resp.get("message", "")
            msg = resp["error"] + ": " + additional_info if additional_info else resp["error"]
            print(f"error getting issuer: {msg}")
            if self.debug:
                print(f"badgr api: {response.text}")
        try:
            assertion = OpenBadgesAssertion(**resp)
        except Exception as e:
            print(f"error parsing issuer: {e}")
            raise e
        return self.cache.set(f"get_assertion_by_id:{assertion_id}", assertion)

    def get_issuer_by_id(self, issuer_id: str) -> OpenBadgesIssuer:
        """Get information for a Badgr Issuer using the Badgr API identifier"""
        # Get from cache if exists
        ret = self.cache.get(f"get_issuer_by_id:{issuer_id}")
        if isinstance(ret, OpenBadgesIssuer):
            return ret
        elif isinstance(ret, dict):
            return OpenBadgesIssuer(**ret)
        # Else get the info from the remote
        endpoint = urljoin(self.api_root_public, f"issuers/{issuer_id}")
        response = requests.get(endpoint, timeout=10)
        response.raise_for_status()
        resp = response.json()
        if "error" in resp:
            additional_info = resp.get("message", "")
            msg = resp["error"] + ": " + additional_info if additional_info else resp["error"]
            print(f"error getting issuer: {msg}")
            if self.debug:
                print(f"badgr api: {response.text}")
        try:
            issuer = OpenBadgesIssuer(
                id=issuer_id,
                description=resp["description"],
                name=resp["name"],
                url=resp["id"],
                image_url=resp["image"],
            )
        except Exception as e:
            print(f"error parsing issuer: {e}")
            raise e
        return self.cache.set(f"get_issuer_by_id:{issuer_id}", issuer)

    def get_badge_by_id(self, badge_id: str) -> OpenBadgesBadgeClass:
        """Get information for a Badgr BadgeClass using the Badgr API identifier"""
        # Get from cache if exists
        ret = self.cache.get(f"get_badge_by_id:{badge_id}")
        if isinstance(ret, OpenBadgesBadgeClass):
            return ret
        elif isinstance(ret, dict):
            return OpenBadgesBadgeClass(**ret)
        # Else get the info from the remote
        endpoint = urljoin(self.api_root_public, f"badges/{badge_id}")
        response = requests.get(endpoint, timeout=10)
        response.raise_for_status()
        resp = response.json()
        if "error" in resp:
            additional_info = resp.get("message", "")
            msg = resp["error"] + ": " + additional_info if additional_info else resp["error"]
            print(f"error getting badge: {msg}")
            if self.debug:
                print(f"badgr api: {response.text}")
        try:
            badge = OpenBadgesBadgeClass(
                id=badge_id,
                description=resp["description"],
                name=resp["name"],
                narrative=resp.get("criteria", {}).get("narrative"),
                issuer_id=resp["issuer"].rsplit("/", 1)[1],
                image_url=resp["image"]["id"],
            )
        except Exception as e:
            print(f"error parsing badgeclass: {e}")
            raise e
        return self.cache.set(f"get_badge_by_id:{badge_id}", badge)

    def get_badges_for_user(self, email: str) -> Optional[List[OpenBadgesAssertion]]:
        """Get information for Badgr Assertions using the recipient's email"""
        # Get from cache if exists
        ret = self.cache.get(f"get_badges_for_user:{email}")
        if isinstance(ret, list):
            assertions: List[OpenBadgesAssertion] = []
            for e in ret:
                if isinstance(e, dict):
                    assertions.append(OpenBadgesAssertion(**e))
                elif isinstance(e, OpenBadgesAssertion):
                    assertions.append(e)
                else:
                    msg = "Unexpected type in get_badges_for_user cache"
                    pmsg = f"{msg}: {e}" if self.debug else msg
                    print(pmsg)
                    raise RuntimeError(msg)
            return assertions
        # Look up badges for user
        response = requests.get(
            urljoin(self.api_root_authenticated, f"issuers/{self.issuer_id}/assertions"),
            headers=self._get_headers(),
            timeout=10,
            params=[('recipient', email)]
        )
        response.raise_for_status()
        resp = response.json()
        # Successful result
        # {"status": {"success": true, "description": "ok"}, "result": []}
        # Failed result
        # {"status": {"success": false, "description": "entity not found or insufficient privileges"}, "result": []}
        if not resp.get("status", {}).get("success", False):
            msg = "error requesting badges for user"
            description = resp.get("status", {}).get("description", "")
            if description:
                msg += ": " + description
            print(msg)
            if self.debug:
                print(f"badgr api: {response.text}")
            return None
        badges: List[OpenBadgesAssertion] = []
        if not resp.get("result"):
            # Cache an empty list, it is a valid response
            return self.cache.set(f"get_badges_for_user:{email}", badges)
        for result in resp.get("result", []):
            try:
                badge_id = result["badgeclass"]
                issuer_id = result["issuer"]
                if not badge_id or not issuer_id:
                    raise TypeError("badgeclass or issuer malformed")
                assertion = OpenBadgesAssertion(
                    id=result["entityId"],
                    accepted=result["acceptance"] == "Accepted",
                    assertion_url=result["openBadgeId"],
                    expires=result["expires"],
                    image_url=result["image"],
                    issued_on=result["issuedOn"],
                    narrative=result["narrative"],
                    recipient_id_type=result["recipient"]["type"],
                    recipient_id=result["recipient"].get("plaintextIdentity", result["recipient"]["identity"]),
                    recipient_name=result.get("extensions", {}).get("extensions:recipientProfile", {}).get("name"),
                    revoked=result["revoked"],
                    revocation_reason=result["revocationReason"],
                    badge_id=badge_id,
                    issuer_id=issuer_id,
                )
                badges.append(assertion)
            except Exception as e:
                print(f"error parsing assertion: {e}")
        return self.cache.set(f"get_badges_for_user:{email}", badges)
