"""OpenBadges Data"""
from typing import List

from pydantic import BaseModel, Field


class OpenBadgesBadgeClass(BaseModel):
    id: str
    description: str | None = None
    name: str
    narrative: str | None = None
    issuer_id: str
    image_url: str


class OpenBadgesIssuer(BaseModel):
    id: str
    description: str
    name: str
    url: str
    image_url: str


class OpenBadgesAssertionEvidence(BaseModel):
    url: str | None = None
    narrative: str | None = None


class OpenBadgesAssertion(BaseModel):
    id: str
    accepted: bool
    assertion_url: str
    expires: str | None = None
    evidence: List[OpenBadgesAssertionEvidence] | None = None
    image_url: str
    issued_on: str
    narrative: str | None = None
    recipient_id_type: str
    recipient_id: str  # This is an arbitrary field, but probably an email
    recipient_name: str | None = None  # this field depends on an API extension
    revocation_reason: str | None = None
    revoked: bool
    badge_id: str
    issuer_id: str
