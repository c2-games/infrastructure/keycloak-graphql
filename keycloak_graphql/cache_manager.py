from datetime import datetime, timedelta
from typing import Dict, Any, Optional
import json

import redis

from pydantic.json import pydantic_encoder


class CacheManager():
    def __init__(self, lifetime=timedelta(minutes=5), **kwargs):
        self.store: Dict[str, Dict[str, Any]] = {}
        self.lifetime: timedelta = lifetime
        self.redis_client: Optional[redis.Redis] = None
        self.debug = kwargs.get("debug", False)
        if kwargs.get("host") and kwargs.get("port") and kwargs.get("db"):
            # Make redis connection here
            self.redis_client = redis.Redis(**kwargs)

    def get(self, key: str, default: Any = None):
        """get a value from the store"""
        now = datetime.now()
        if not self.redis_client:
            pack = self.store.get(key, {"value": default, "expiry": now + self.lifetime})
            if pack["expiry"] > now:
                ret = pack.get("value")
                try:
                    if isinstance(ret, str):
                        ret = json.loads(ret)
                        if self.debug:
                            print(f"cache hit for {key}")
                    else:
                        ret = default
                except:
                    print("error decoding value stored in json")
                    ret = default
            else:
                ret = default
        else:
            ret = self.redis_client.get(key)
            if ret is None:
                ret = default
        return ret

    def set(self, key: str, value: Any):
        """set a value in the store"""
        now = datetime.now()
        if not self.redis_client:
            self.store[key] = {
                "value": json.dumps(value, default=pydantic_encoder),
                "expiry": now + self.lifetime}
        else:
            self.redis_client.set(key, json.dumps(value, default=pydantic_encoder))
            self.redis_client.expire(key, self.lifetime)
        return value
