from typing import Tuple, Any
from fastapi import APIRouter, Response, status

from ..schema import KCB, GQLB

router = APIRouter()


@router.get("/livez")
def livez_check() -> dict:
    """Report liveness of the service."""
    return {"status": "ok"}


@router.get("/readyz")
async def readyz_check(verbose: Any = None, response: Response = Response()) -> dict:
    """Report if the service is ready to accept requests."""
    ret = {"status": "ok"}
    ready = True

    okay, message = keycloak_ready()
    ret["keycloak"] = message
    ready = ready and okay

    okay, message = await db_ready()
    ret["db"] = message
    ready = ready and okay

    if not ready:
        ret["status"] = "not ready"
        response.status_code = status.HTTP_503_SERVICE_UNAVAILABLE

    if verbose is not None:
        return ret

    return {"status": ret["status"]}


def keycloak_ready() -> Tuple[bool, str]:
    """Test if keycloak is ready."""
    try:
        KCB.client_token()
        return True, "ok"
    except:  # pylint: disable=bare-except
        return False, "client token not available"


async def db_ready() -> Tuple[bool, str]:
    """Test if the database is ready."""
    q = """
        query ReadyCheck {
            events { id }
        }
    """
    try:
        result = await GQLB.query(q)
        if not isinstance(result.get("events"), list):
            return (False, "database did not return expected results")
    except:  # pylint: disable=bare-except
        return (False, "could not complete query")
    return (True, "ok")
