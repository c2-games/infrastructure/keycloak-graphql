from datetime import timedelta
from typing import Any, Optional

from pytimeparse.timeparse import timeparse
from cincoconfig import Field, IntField, Schema, Config
from cincoconfig.fields import BoolField, SecureField, StringField, UrlField


class DurationField(Field):
    """
    A human readable duration field. Values are validated that they parse to timedelta.
    """

    storage_type = timedelta

    def __init__(self, **kwargs):
        """
        Override to make sure default is valid
        """
        value = kwargs.get("default")
        if value is not None:
            value = self._convert(value)
            kwargs["default"] = value
        super().__init__(**kwargs)

    def _convert(self, value: Any) -> timedelta:
        if isinstance(value, timedelta):
            return value
        elif isinstance(value, str):
            v = timeparse(value)
            if v is None:
                raise ValueError("value can not be parsed by any format")
            return timedelta(seconds=float(v))
        elif isinstance(value, float):
            return timedelta(seconds=value)
        elif isinstance(value, int):
            return timedelta(seconds=float(value))

        raise ValueError(f"value must be timedelta, str or number, not {type(value).__name__}")

    def _validate(self, cfg: Config, value: Any) -> timedelta:
        return self._convert(value)

    def to_basic(self, cfg: Config, value: timedelta) -> str:
        return str(value)

    def to_python(self, cfg: Config, value: str) -> Optional[timedelta]:
        return self._convert(value)


config_schema = Schema(env="KCGQL")
config_schema.badgr.username = StringField(required=True)
config_schema.badgr.password = SecureField(required=True)
config_schema.badgr.issuer_id = StringField(required=True)
config_schema.badgr.api_root_public = UrlField(required=True)
config_schema.badgr.api_root_authenticated = UrlField(required=True)
config_schema.badgr.cache_lifetime = DurationField(default="2h")
config_schema.badges.names.competitor_2025 = StringField(default="2025 Competitor")
config_schema.badges.names.competitor_2024 = StringField(default="2024 Competitor")
config_schema.badges.names.most_valuable = StringField(default="Most Valuable Teammate")
config_schema.badges.names.most_improved = StringField(default="Most Improved Teammate")
config_schema.badges.names.regional_champion = StringField(default="Regional Champion")
config_schema.badges.names.invitational_champion = StringField(default="Invitational Champion")
config_schema.badges.names.invitational_competitor = StringField(default="Invitational Competitor")
config_schema.kc.client_id = StringField(required=True)
config_schema.kc.client_secret = SecureField(required=True)
config_schema.kc.realm = StringField(required=True)
config_schema.kc.server = UrlField(required=True)
config_schema.kc.email_lifetime = DurationField(default="7d")
config_schema.gql.url = UrlField(required=True)
config_schema.debug = BoolField(default=False)
config_schema.introspection = BoolField(default=True)
config_schema.playground = BoolField(default=False)
config_schema.cache_lifetime = DurationField(default="5m")
config_schema.censor_back = IntField(default=0, min=0)
config_schema.censor_front = IntField(default=0, min=0)
config_schema.host = StringField(default="0.0.0.0")
config_schema.port = IntField(default=8000)
config = config_schema()


def get_config() -> Config:
    """Get the validated config."""
    config.validate()
    return config
