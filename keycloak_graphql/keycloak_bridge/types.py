from typing import Any, Optional, List, Dict, Tuple
from pydantic import BaseModel


class UserAttributes(BaseModel):
    competition_role: Optional[str] = None
    store_url: Optional[str] = None
    mvt_url: Optional[str] = None
    mit_url: Optional[str] = None


class User(BaseModel):
    id: str
    email: str
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    discord_linked: bool = False
    discord_id: Optional[str] = None
    discord_username: Optional[str] = None
    attributes: Optional[UserAttributes] = None


class InviteResult(BaseModel):
    user: Optional[User] = None
    expiration: int = 0
    error: Optional[str] = None


# Keycloak library types
class UserConsentRepresentation(BaseModel):
    clientId: Optional[str] = None
    createdDate: Optional[int] = None
    grantedClientScopes: Optional[List[str]] = None
    lastUpdatedDate: Optional[int] = None


class CredentialRepresentation(BaseModel):
    createdDate: Optional[int] = None
    credentialData: Optional[str] = None
    id: Optional[str] = None
    priority: Optional[int] = None
    secretData: Optional[str] = None
    temporary: Optional[bool] = None
    type: Optional[str] = None
    userLabel: Optional[str] = None
    value: Optional[str] = None


class FederatedIdentityRepresentation(BaseModel):
    identityProvider: Optional[str] = None
    userId: Optional[str] = None
    userName: Optional[str] = None


class UserRepresentation(BaseModel):
    access: Optional[Dict[str, Any]] = None
    attributes: Optional[Dict[str, Any]] = None
    clientConsents: Optional[List[UserConsentRepresentation]] = None
    clientRoles: Optional[Dict[str, Any]] = None
    createdTimestamp: Optional[int] = None
    credentials: Optional[List[CredentialRepresentation]] = None
    disableableCredentialTypes: Optional[List[str]] = None
    email: Optional[str] = None
    emailVerified: Optional[bool] = None
    enabled: Optional[bool] = None
    federatedIdentities: Optional[List[FederatedIdentityRepresentation]] = None
    federationLink: Optional[str] = None
    firstName: Optional[str] = None
    groups: Optional[List[str]] = None
    id: Optional[str] = None
    lastName: Optional[str] = None
    notBefore: Optional[int] = None
    origin: Optional[str] = None
    realmRoles: Optional[List[str]] = None
    requiredActions: Optional[List[str]] = None
    self: Optional[str] = None
    serviceAccountClientId: Optional[str] = None
    username: Optional[str] = None

    def kc_user_discord_id(self) -> Optional[Tuple[str, str]]:
        """Get discord federated identity information if available"""
        ret: Optional[Tuple[str, str]] = None
        for v in self.federatedIdentities or []:
            if not v.identityProvider or v.identityProvider.lower() != "discord":
                continue
            if not (v.userId and v.userName):
                print(f"{v.identityProvider} id({v.userId}) or username({v.userName}) is null for user_id({self.id})")
                continue
            ret = (v.userId, v.userName)
        return ret

    def _parse_user_attributes(self) -> Optional[UserAttributes]:
        """
        Parse out select attributes

        Note that attributes in keycloak are by default a list because multiple
        entries for the same key can exist.
        """
        if not self.attributes:
            return None
        attributes = UserAttributes()
        comp_role = self.attributes.get('comp_role')
        if comp_role:
            attributes.competition_role = comp_role[0]
        storeurl = self.attributes.get('storeurl')
        if storeurl:
            attributes.store_url = storeurl[0]
        mvturl = self.attributes.get('mvturl')
        if mvturl:
            attributes.mvt_url = mvturl[0]
        miturl = self.attributes.get('miturl')
        if miturl:
            attributes.mit_url = miturl[0]
        return attributes

    def user_from_kc_user(self) -> Optional[User]:
        """Extract necessary info from keycloak's user type to create the GQL User"""
        if not (self.id and self.email):
            print(f"failed to cast kc user id({self.id}) and email({self.email}) which is invalid)")
            return None
        discord_info = self.kc_user_discord_id()
        return User(
            id=self.id,
            email=self.email,
            first_name=self.firstName,
            last_name=self.lastName,
            discord_linked=bool(discord_info),
            discord_id=discord_info[0] if discord_info else None,
            discord_username=discord_info[1] if discord_info else None,
            attributes=self._parse_user_attributes()
        )
