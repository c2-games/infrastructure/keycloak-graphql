from typing import Any, Dict, Optional, List
import traceback

import validators

from keycloak import KeycloakAdmin

from .types import InviteResult, User, UserRepresentation
from ..cache_manager import CacheManager


class KeycloakBridge:
    def __init__(
        self,
        client_id: str,
        client_secret: str,
        realm: str,
        server: str,
        debug: bool = False,
        email_lifetime: int = 259200,
        **kwargs,
    ):
        self._kca = None
        self.client_id = client_id
        self.client_secret = client_secret
        self.realm = realm
        self.server = server
        self.debug = debug
        self.email_lifetime = email_lifetime
        self.cache = CacheManager(**kwargs)

    @property
    def keycloak_admin(self) -> KeycloakAdmin:
        """Access the initialized KeycloakAdmin interface"""
        if not self._kca:
            self._kca = KeycloakAdmin(
                server_url=self.server,
                client_id=self.client_id,
                client_secret_key=self.client_secret,
                realm_name=self.realm,
                verify=True,
                auto_refresh_token=["get", "put", "post", "delete"],
            )
        return self._kca

    def client_token(self) -> Optional[str]:
        """Use the admin client token."""
        if self.keycloak_admin and self.keycloak_admin.token:
            return self.keycloak_admin.token.get("access_token", "")
        return None

    def decode_token(self, token):
        """Decode and validate a JWT for this keycloak instance"""
        if not self.keycloak_admin.connection:
            raise ConnectionError("Keycloak connection is not initialized")
        # pylint: disable=consider-using-f-string
        public_key = "-----BEGIN PUBLIC KEY-----\n{}\n-----END PUBLIC KEY-----".format(
            self.keycloak_admin.connection.keycloak_openid.public_key()
        )
        options = {"verify_signature": True, "verify_aud": False, "verify_exp": True}
        return self.keycloak_admin.connection.keycloak_openid.decode_token(
            token, key=public_key, options=options
        )

    def _search_by_email(
        self,
        search: str,
        exact: bool = False,
        email_verified: bool = True,
    ) -> List[User]:
        ret = self.cache.get(f"_search_by_email:{search}:{str(exact)}:{str(email_verified)}")
        if isinstance(ret, list):
            return ret
        results: List[Dict[str, Any]] = self.keycloak_admin.get_users(query={
            "email": search,
            "exact": exact,
            "emailVerified": email_verified,
        })
        ret = []
        for kc_user in results:
            if not kc_user:
                continue
            user = UserRepresentation(**kc_user).user_from_kc_user()
            if not user:
                print("search returned invalid user")
                if self.debug:
                    traceback.print_exc()
                continue
            ret.append(user)
        return self.cache.set(
            f"_search_by_email:{search}:{str(exact)}:{str(email_verified)}",
            ret,
        )

    def invite_user(
        self,
        email: str,
        first_name: str,
        last_name: str,
        phone_number: Optional[str] = None,
    ) -> InviteResult:
        """Create a keycloak account if this user doesn't exist and force a password update email."""
        user = self.user_by_email(email)
        if user:
            return InviteResult(error="user already exists")
        step = "creation"
        try:
            user_data = {
                "email": email,
                "firstName": first_name,
                "lastName": last_name,
                "emailVerified": False,
                "enabled": True,
            }
            if phone_number:
                user_data["attributes"] = {
                    "phone_number": phone_number
                }
            user_id = self.keycloak_admin.create_user(user_data)
            if not user_id:
                return InviteResult(error="user creation failed")
            step = "notification"
            self.keycloak_admin.send_update_account(
                user_id,
                payload=["UPDATE_PASSWORD", "UPDATE_PROFILE"],
                lifespan=self.email_lifetime,
            )
        except Exception as e:
            print(f"Could not create user {email}: {e}")
            if self.debug:
                traceback.print_exc()
            return InviteResult(error=f"user {step} failed")
        return InviteResult(
            user=self.user_by_id(user_id),
            expiration=self.email_lifetime
        )

    def users_by_domain(self, domain: str) -> List[User]:
        """Search for users with a particular email domain"""
        try:
            if not validators.domain(domain):
                return []
            return self._search_by_email(f"@{domain}")
        except validators.ValidationError:
            return []

    def user_by_id(self, uuid: str) -> Optional[User]:
        """Retrieve a user by keycloak uuid"""
        ret = self.cache.get(f"user_by_id:{uuid}")
        if isinstance(ret, dict):
            return User(**ret)
        elif isinstance(ret, User):
            return ret
        kc_user: Dict[str, Any] = self.keycloak_admin.get_user(uuid)
        if not kc_user:
            return None
        user = UserRepresentation(**kc_user).user_from_kc_user()
        if not user:
            return None
        return self.cache.set(
            f"user_by_id:{uuid}",
            user,
        )

    def user_id_by_username(self, username: str) -> Optional[str]:
        """Retrieve a keycloak user uuid by keycloak username"""
        ret = self.cache.get(f"user_id_by_username:{username}")
        if isinstance(ret, str):
            return ret
        user: Optional[str] = self.keycloak_admin.get_user_id(username)
        if not user:
            return None
        return self.cache.set(
            f"user_id_by_username:{username}",
            user,
        )

    def user_by_email(self, email: str) -> Optional[User]:
        """
        Retrieve a user by email

        Currently this just uses the email as the username, not actually making
        a query against the email field in keycloak.
        """
        # All functions here use caching, so no need to duplicate
        user_id = self.user_id_by_username(email)
        if not user_id:
            return None
        user = self.user_by_id(user_id)
        if not user:
            return None
        return user
