from typing import List, Optional, Union
import traceback

from graphql.type import GraphQLResolveInfo
from ariadne_graphql_modules import ObjectType, DeferredType, gql
import starlette.requests
from keycloak_graphql.cache_manager import CacheManager

from keycloak_graphql.config import get_config

from .keycloak_bridge import KeycloakBridge
from .keycloak_bridge.types import User, InviteResult
from .gql_bridge import GqlBridge
from .openbadges import OpenBadgesAssertion
from .openbadges.badgr import BadgrProvider, MutationResult

config = get_config()

KCB = KeycloakBridge(
    client_id=config.kc.client_id,
    client_secret=config.kc.client_secret,
    realm=config.kc.realm,
    server=config.kc.server,
    debug=config.debug,
    email_lifetime=int(config.kc.email_lifetime.total_seconds()),
    lifetime=config.cache_lifetime,
)
GQLB = GqlBridge(
    gql_url=config.gql.url,
    kcb=KCB,
    debug=config.debug,
    lifetime=config.cache_lifetime,
)
BADGR = BadgrProvider(
    kcb=KCB,
    gqlb=GQLB,
    username=config.badgr.username,
    password=config.badgr.password,
    issuer_id=config.badgr.issuer_id,
    api_root_public=config.badgr.api_root_public,
    api_root_authenticated=config.badgr.api_root_authenticated,
    cache=CacheManager(lifetime=config.badgr.cache_lifetime, debug=config.debug),
    debug=config.debug,
    # badge names
    competitor_2025=config.badges.names.competitor_2025,
    competitor_2024=config.badges.names.competitor_2024,
    most_valuable=config.badges.names.most_valuable,
    most_improved=config.badges.names.most_improved,
    regional_champion=config.badges.names.regional_champion,
    invitational_champion=config.badges.names.invitational_champion,
    invitational_competitor=config.badges.names.invitational_competitor,
)


def get_jwt(request: starlette.requests.Request):
    """Get the JWT Bearer token from the request header"""
    if not request:
        return None
    auth = request.headers.get("Authorization", "").split()
    prefix = "Bearer"
    if len(auth) != 2 or auth[0].lower() != prefix.lower():
        return None
    return auth[1]


# Add the parsed jwt into the GraphQL request context
def get_parsed_jwt(request: starlette.requests.Request):
    """Get the JWT then parse and validate it"""
    token = get_jwt(request)
    if not token:
        return {"request": request}
    try:
        parsed = KCB.decode_token(token)
    except:
        return {"request": request}
    return {"request": request, "parsed_token": parsed}


def get_hasura_roles(info: GraphQLResolveInfo) -> List[str]:
    """Get the list of hasura allowed roles from JWT claims"""
    token = info.context.get("parsed_token")
    if not token:
        return []
    allowed_roles = token.get("https://hasura.io/jwt/claims", {}).get(
        "x-hasura-allowed-roles", []
    )
    return allowed_roles


def is_self(user: User, info: GraphQLResolveInfo) -> bool:
    """Check if user is requesting info on self"""
    token = info.context.get("parsed_token")
    if not token:
        return False
    if user.id == token.get("sub", ""):
        return True
    return False


async def is_teammate(user: User, info: GraphQLResolveInfo) -> bool:
    """Check if user is requesting info on a teammate"""
    token = info.context.get("parsed_token")
    if not token:
        return False
    req_id = token.get("sub")
    if req_id:
        return await GQLB.are_teammates(req_id, user.id)
    return False


async def same_institution(user: User, info: GraphQLResolveInfo) -> bool:
    """Check if user is requesting info on someone in the same institution"""
    return await same_institution_email(user.email, info)


async def same_institution_email(email: str, info: GraphQLResolveInfo) -> bool:
    """Check if user is requesting info has same institution as email"""
    token = info.context.get("parsed_token")
    if not token:
        return False
    req_email = token.get("email")
    if req_email:
        return await GQLB.same_institution(req_email, email)
    return False


def is_hasura_staff(info) -> bool:
    """Check if requester is staff based on hasura roles"""
    roles = get_hasura_roles(info)
    if "staff" in roles:
        return True
    elif "admin" in roles:
        return True
    elif "event_admin" in roles:
        return True
    elif "ctf_admin" in roles:
        return True
    elif "attacker" in roles:
        return True
    return False


def is_hasura_admin(info) -> bool:
    """Check if requester is admin based on hasura roles"""
    roles = get_hasura_roles(info)
    if "admin" in roles:
        return True
    elif "event_admin" in roles:
        return True
    return False


def censor_string(sensitive: str, front: int, back: int) -> str:
    """Censor a string leaving some characters visible at the front and back"""
    # treat negative front values as 0
    front = max(front, 0)

    ret = sensitive[:front] + "*****"

    # If 0 (or less), no additional characters are displayed
    if back <= 0:
        return ret

    return ret + sensitive[-back:]


def censor_email(sensitive: str, front: int, back: int) -> str:
    """Censor an email leaving some characters visible along with the domain"""
    parts = sensitive.split("@", 1)
    parts[0] = censor_string(parts[0], front, back)
    return "@".join(parts)


class BadgeClassType(ObjectType):
    """Type for openbadges badge information"""
    # pylint: disable=redefined-builtin
    __schema__ = gql(
        """
        type BadgeClass {
            id: String!
            description: String
            name: String!
            narrative: String
            issuer_id: String!
            image_url: String!
        }
        """
    )


class BadgeIssuerType(ObjectType):
    """Type for openbadges issuer information"""
    # pylint: disable=redefined-builtin
    __schema__ = gql(
        """
        type BadgeIssuer {
            id: String!
            description: String!
            name: String!
            url: String!
            image_url: String!
        }
        """
    )


class BadgeAssertionType(ObjectType):
    """Type for openbadges user awarded badges"""
    # pylint: disable=redefined-builtin
    # Disable pylint warning for unused info arguments
    # pylint: disable=unused-argument
    __schema__ = gql(
        """
        type BadgeAssertion {
            id: String!
            accepted: Boolean!
            assertion_url: String!
            expires: String
            image_url: String!
            issued_on: String!
            narrative: String
            recipient_id_type: String!
            recipient_id: String!
            recipient_name: String
            revocation_reason: String
            revoked: Boolean!
            badge_id: String!
            badge: BadgeClass!
            issuer_id: String!
            issuer: BadgeIssuer!
        }
        """
    )
    __requires__ = [DeferredType("BadgeClass"), DeferredType("BadgeIssuer")]

    @staticmethod
    async def resolve_badge(assertion: Union[OpenBadgesAssertion, dict], info: GraphQLResolveInfo):
        """Request detailed badge information"""
        if isinstance(assertion, dict):
            assertion = OpenBadgesAssertion(**assertion)
        try:
            return BADGR.get_badge_by_id(assertion.badge_id)
        except Exception as e:
            print(f"Error resolving attribute: badge ({assertion.badge_id}): {e}")
            if config.debug:
                traceback.print_exc()
            # Don't raise from e because it would give out extra information to the user
            # pylint: disable=raise-missing-from
            raise RuntimeError("Error resolving attribute: badge")

    @staticmethod
    async def resolve_issuer(assertion: Union[OpenBadgesAssertion, dict], info: GraphQLResolveInfo):
        """Request detailed issuer information"""
        if isinstance(assertion, dict):
            assertion = OpenBadgesAssertion(**assertion)
        try:
            return BADGR.get_issuer_by_id(assertion.issuer_id)
        except Exception as e:
            print(f"Error resolving attribute: issuer ({assertion.issuer_id}): {e}")
            if config.debug:
                traceback.print_exc()
            # Don't raise from e because it would give out extra information to the user
            # pylint: disable=raise-missing-from
            raise RuntimeError("Error resolving attribute: issuer")


class UserAttributesType(ObjectType):
    """Keycloak User Attributes"""

    __schema__ = gql(
        """
        type UserAttributes {
            competition_role: String
            store_url: String
            mvt_url: String
            mit_url: String
        }
        """
    )


class UserType(ObjectType):
    """Keycloak Users"""

    __schema__ = gql(
        """
        type User {
            id: ID!
            first_name: String
            last_name: String
            email: String
            discord_linked: Boolean!
            discord_id: String
            discord_username: String
            badges: [BadgeAssertion!]
            attributes: UserAttributes
        }
        """
    )
    __requires__ = [DeferredType("BadgeAssertion"), DeferredType("UserAttributes")]

    @staticmethod
    async def _should_censor(user: User, info: GraphQLResolveInfo):
        if is_self(user, info) or is_hasura_staff(info):
            return False
        # This can be expensive, so wait until other options are checked
        if await is_teammate(user, info):
            return False
        elif await same_institution(user, info):
            return False
        return True

    @staticmethod
    async def resolve_first_name(user: Union[User, dict], info: GraphQLResolveInfo):
        """Only give out first_name to user, staff or teammate"""
        if isinstance(user, dict):
            user = User(**user)
        if not await UserType._should_censor(user, info):
            return user.first_name
        if not user.first_name:
            return censor_string("", 0, 0)
        return censor_string(user.first_name, config.censor_front, config.censor_back)

    @staticmethod
    async def resolve_last_name(user: Union[User, dict], info: GraphQLResolveInfo):
        """Only give out last_name to user, staff or teammate"""
        if isinstance(user, dict):
            user = User(**user)
        if not await UserType._should_censor(user, info):
            return user.last_name
        if not user.last_name:
            return censor_string("", 0, 0)
        return censor_string(user.last_name, config.censor_front, config.censor_back)

    @staticmethod
    async def resolve_email(user: Union[User, dict], info: GraphQLResolveInfo):
        """Only give out email to user, staff or teammate"""
        if isinstance(user, dict):
            user = User(**user)
        if not await UserType._should_censor(user, info):
            return user.email
        if not user.email:
            return censor_email("", 0, 0)
        return censor_email(user.email, config.censor_front, config.censor_back)

    @staticmethod
    async def resolve_discord_id(user: Union[User, dict], info: GraphQLResolveInfo):
        """Only give out discord_id to user, staff or teammate"""
        if isinstance(user, dict):
            user = User(**user)
        if not user.discord_id:
            return None
        if not await UserType._should_censor(user, info):
            return user.discord_id
        return censor_string(user.discord_id, config.censor_front, config.censor_back)

    @staticmethod
    async def resolve_discord_username(user: Union[User, dict], info: GraphQLResolveInfo):
        """Only give out discord_username to user, staff or teammate"""
        if isinstance(user, dict):
            user = User(**user)
        if not user.discord_username:
            return None
        if not await UserType._should_censor(user, info):
            return user.discord_username
        return censor_string(user.discord_username, config.censor_front, config.censor_back)

    @staticmethod
    async def resolve_badges(user: Union[User, dict], info: GraphQLResolveInfo):
        """Only give out email to user, staff or teammate"""
        if isinstance(user, dict):
            user = User(**user)
        # It is possible the user email isn't set, but unlikely
        if not user.email:
            return []
        # Do censoring here, because it would be harder to do it on badges right now
        if await UserType._should_censor(user, info):
            return []
        try:
            badges = BADGR.get_badges_for_user(user.email)
        except Exception as e:
            print(f"Could not retrieve badges for {user.email}: {e}")
            if config.debug:
                traceback.print_exc()
            badges = []
        return badges or []

    @staticmethod
    async def resolve_attributes(user: Union[User, dict], info: GraphQLResolveInfo):
        """Only give out attributes to the user themselves and staff"""
        if isinstance(user, dict):
            user = User(**user)
        if is_self(user, info) or is_hasura_staff(info):
            return user.attributes
        return None


class UsersQueries(ObjectType):
    """Queries for keycloak users"""

    # pylint: disable=redefined-builtin
    __schema__ = gql(
        """
        type Query {
            c2games_user_by_pk(id: ID!): User
            c2games_users_by_domain(domain: String!): [User!]!
            c2games_user_by_email(email: String!): User
        }
        """
    )
    __requires__ = [DeferredType("User")]

    @staticmethod
    def resolve_c2games_user_by_pk(*_, id: str) -> Optional[User]:
        """Get keycloak user by id"""
        try:
            return KCB.user_by_id(id)
        except Exception as e:
            print(f"Could not lookup user {id}: {e}")
            if config.debug:
                traceback.print_exc()
            return None  # Don't return a user if an error happens

    @staticmethod
    def resolve_c2games_users_by_domain(*_, domain: str) -> List[User]:
        """Get keycloak user by id"""
        try:
            return KCB.users_by_domain(domain)
        except Exception as e:
            print(f"Could not lookup users by domain {domain}: {e}")
            if config.debug:
                traceback.print_exc()
            return []  # Don't return a user if an error happens

    @staticmethod
    def resolve_c2games_user_by_email(*_, email: str) -> Optional[User]:
        """Get keycloak user by email"""
        return KCB.user_by_email(email)


class UsersInviteType(ObjectType):
    """Type for user invites"""
    # pylint: disable=redefined-builtin
    __schema__ = gql(
        """
        type InviteResult {
            user: User
            expiration: Int
            error: String
        }
        """
    )
    __requires__ = [DeferredType("User")]


class MutationResultType(ObjectType):
    """Type for mutation results"""

    # pylint: disable=redefined-builtin
    __schema__ = gql(
        """
        type MutationResult {
            success: Boolean!
            modified: Boolean!
            error: String
        }
        """
    )


class UsersMutations(ObjectType):
    """Mutations for keycloak users"""

    # pylint: disable=redefined-builtin
    __schema__ = gql(
        """
        type Mutation {
            c2games_user_invite(
                email: String!,
                first_name: String!,
                last_name: String!,
                phone_number: String = null,
            ): InviteResult
        }
        """
    )
    __requires__ = [DeferredType("InviteResult")]

    @staticmethod
    async def resolve_c2games_user_invite(_, info, email, first_name, last_name, phone_number) -> InviteResult:
        """Invite a user by creating a keycloak account."""
        try:
            if not is_hasura_admin(info):
                # Restrict this to only allow invites to people from the same institution
                allowed = await same_institution_email(email, info)
                if not allowed:
                    print(f"Refusing to invite user {email}, {first_name} {last_name}: not same institution")
                    return InviteResult(error="user is not from the same institution")
            return KCB.invite_user(email, first_name, last_name, phone_number)
        except Exception as e:
            print(f"Could not invite user {email}, {first_name} {last_name}: {e}")
            if config.debug:
                traceback.print_exc()
            return InviteResult(error="internal error")  # Don't return a user if an error happens


class Assert2024BadgeMutation(ObjectType):
    """Mutations for asserting a badge in badgr"""

    # pylint: disable=redefined-builtin
    __schema__ = gql(
        """
        type Mutation {
            c2games_assert_2024_badge(assertion_id: ID!): MutationResult
        }
        """
    )
    __requires__ = [DeferredType("MutationResult")]

    @staticmethod
    async def resolve_c2games_assert_2024_badge(_, info, assertion_id: str) -> MutationResult:
        """Assert a badge in Badgr from the 2024 NCAE Season."""
        if not is_hasura_admin(info):
            return MutationResult(success=False, modified=False, error="not authorized")

        try:
            return await BADGR.assert_badgr_2024(assertion_id)
        except Exception as e:
            print(f"unexpected error asserting badge in Badgr: {e}")
            if config.debug:
                traceback.print_exc()
            return MutationResult(success=False, modified=False, error="internal error")


class Assert2025BadgeMutation(ObjectType):
    """Mutations for asserting a badge in badgr"""

    # pylint: disable=redefined-builtin
    __schema__ = gql(
        """
        type Mutation {
            c2games_assert_2025_badge(assertion_id: ID!): MutationResult
        }
        """
    )
    __requires__ = [DeferredType("MutationResult")]

    @staticmethod
    async def resolve_c2games_assert_2025_badge(_, info, assertion_id: str) -> MutationResult:
        """Assert a badge in Badgr from the 2025 NCAE Season."""
        if not is_hasura_admin(info):
            return MutationResult(success=False, modified=False, error="not authorized")

        try:
            return await BADGR.assert_badgr_2025(assertion_id)
        except Exception as e:
            print(f"unexpected error asserting badge in Badgr: {e}")
            if config.debug:
                traceback.print_exc()
            return MutationResult(success=False, modified=False, error="internal error")
