from starlette.requests import Request
from starlette.responses import Response
from ariadne.asgi import GraphQL
from ariadne.asgi.handlers import GraphQLHTTPHandler
from ariadne_graphql_modules import make_executable_schema

from .schema import (
    BadgeClassType,
    BadgeIssuerType,
    BadgeAssertionType,
    UserType,
    UserAttributesType,
    UsersInviteType,
    UsersQueries,
    UsersMutations,
    get_parsed_jwt,
    Assert2025BadgeMutation,
    Assert2024BadgeMutation,
    MutationResultType,
)


# Override the base class to keep introspection, but disable the playground
class GraphQLHTTPHandlerNoPlayground(GraphQLHTTPHandler):
    # Don't allow playground
    async def render_playground(
        self, request: Request
    ) -> Response:
        return self.handle_not_allowed_method(request)


def make_gql_app(debug=False, playground=False, introspection=False):
    """Make an ASGI GraphQL app"""
    schema = make_executable_schema(
        BadgeClassType,
        BadgeIssuerType,
        BadgeAssertionType,
        MutationResultType,
        UserAttributesType,
        UserType,
        UsersInviteType,
        UsersQueries,
        UsersMutations,
        Assert2024BadgeMutation,
        Assert2025BadgeMutation
    )
    # If None, use the default handler which enables the playground if
    # introspection is enabled
    http_handler = None
    if not playground:
        if introspection:
            print("warning: introspection enabled but playground disabled")
        http_handler = GraphQLHTTPHandlerNoPlayground()
    elif playground and not introspection:
        print("warning: introspection disabled, playground will still be disabled")
    return GraphQL(
        schema=schema,
        debug=debug,
        introspection=introspection,
        context_value=get_parsed_jwt,
        http_handler=http_handler
    )
