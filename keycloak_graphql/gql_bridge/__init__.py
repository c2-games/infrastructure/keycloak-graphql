from typing import Optional, Dict, List, Any, Union

import gql
from graphql import DocumentNode
from gql.transport.aiohttp import AIOHTTPTransport
from .types import AwardAssertion

from ..cache_manager import CacheManager
from ..keycloak_bridge import KeycloakBridge


class GqlBridge:
    def __init__(self, gql_url: str, kcb: KeycloakBridge, **kwargs):
        self.gql_url = gql_url
        self.kcb = kcb
        self.debug = kwargs.get("debug", False)
        self.cache = CacheManager(**kwargs)

    def _get_client_transport(self):
        token = self.kcb.client_token()
        headers = {}
        if token:
            headers["Authorization"] = f'Bearer {token}'
            headers["x-hasura-role"] = 'event_admin'
        return AIOHTTPTransport(
            url=self.gql_url,
            headers=headers
        )

    async def query(
        self, q: Union[str, DocumentNode], variables: Optional[dict] = None
    ) -> Dict[str, Any]:
        """Make a GraphQL query."""
        if isinstance(q, str):
            if self.debug:
                print(f"GQL query {q}")
            q = gql.gql(q)
        # Using `async with` on the client will start a connection on the transport
        # and provide a `session` variable to execute queries on this connection
        async with gql.Client(
            transport=self._get_client_transport(),
            fetch_schema_from_transport=False,
        ) as session:
            if variables is not None:
                return await session.execute(q, variable_values=variables)
            else:
                return await session.execute(q)

    async def team_id_by_user_id(self, uuid: str) -> List[str]:
        """Get the team_id for a user by their keycloak uuid."""
        ret = self.cache.get(f"team_id_by_user_id:{uuid}")
        if isinstance(ret, list):
            return ret
        q = """
            query TeamIdByUserId($user_id: uuid) {
                team_membership(where: {user_id: {_eq: $user_id}}) {
                    team_id
                }
            }
            """
        result = await self.query(q, {"user_id": uuid})
        if result and result["team_membership"]:
            ret = [r["team_id"] for r in result["team_membership"]]
        else:
            ret = []
        return self.cache.set(f"team_id_by_user_id:{uuid}", ret)

    async def get_award_assertion(self, assertion_id: str) -> Optional[AwardAssertion]:
        """
        Get an award assertion and it's supporting data by ID.
        :param assertion_id: UUID of the assertion in the Hasura database.
        """
        # this data is not cached to prevent issues with calling the badgr api with a duplicate assertion
        # ret = self.cache.get(f"get_award_assertion:{assertion_id}")
        # if isinstance(ret, AwardAssertion):
        #     return ret
        # elif isinstance(ret, dict):
        #     return AwardAssertion(**ret)
        # else:
        #     ret = None

        q = """
        query AssertionById($assertion_id: uuid!) {
            awards_assertions_by_pk(id: $assertion_id) {
                id
                badge_id
                event_id
                team_id
                user_id
                created
                badgr_assertion_id
                event {
                    name
                    start
                    event_group {
                        name
                    }
                }
                team {
                    id
                    name
                    long_name
                    institution {
                        name
                    }
                    team_size
                }
                badge {
                    name
                    img
                    img_dark
                    badgr_badge_id
                }
            }
        }"""

        result = await self.query(q, {"assertion_id": assertion_id})
        if result and result["awards_assertions_by_pk"]:
            user = self.kcb.user_by_id(result["awards_assertions_by_pk"]["user_id"])
            if not user:
                print('Could not lookup user by ID')
                raise RuntimeError("Could not lookup user by ID")
            return AwardAssertion(user=user, **result["awards_assertions_by_pk"])
        return None

    async def update_award_assertion(self, assertion_id: str, **update):
        """Update an Award Assertion. Each keyword argument is a field to update."""
        q = """
        mutation UpdateAssertion($assertion_id: uuid!, $update: awards_assertions_set_input!) {
          update_awards_assertions_by_pk(pk_columns: {id: $assertion_id}, _set: $update) {
              id
          }
        }
        """
        return await self.query(q, {"assertion_id": assertion_id, "update": update})

    async def are_teammates(self, user1: str, user2: str) -> bool:
        """Check if two users are on the same team."""
        user1_teams = await self.team_id_by_user_id(user1)
        if not user1_teams:
            return False
        user2_teams = await self.team_id_by_user_id(user2)
        if not user2_teams:
            return False
        for team in user1_teams:
            if team in user2_teams:
                return True
        return False

    async def institution_id_by_domain(self, domain: str) -> List[str]:
        """Get institution_id's for an email domain."""
        ret = self.cache.get(f"institution_id_by_domain:{domain}")
        if isinstance(ret, list):
            return ret
        q = """
            query InstitutionIdByDomain($domain: String) {
                institution_domains(where: {domain: {_eq: $domain}}) {
                    institution_id
                }
            }
            """
        result = await self.query(q, {"domain": domain})
        if result and result["institution_domains"]:
            ret = [r["institution_id"] for r in result["institution_domains"]]
        else:
            ret = []
        return self.cache.set(f"institution_id_by_domain:{domain}", ret)

    async def same_institution(self, email1: str, email2: str) -> bool:
        """Check if two emails belong to the same institution."""
        domain1 = email1.split('@', 1)[-1]
        domain2 = email2.split('@', 1)[-1]
        institutions1 = await self.institution_id_by_domain(domain1)
        if not institutions1:
            return False
        institutions2 = await self.institution_id_by_domain(domain2)
        if not institutions2:
            return False
        for institution in institutions1:
            if institution in institutions2:
                return True
        return False
