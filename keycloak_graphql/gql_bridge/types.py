from pydantic import BaseModel

from keycloak_graphql.keycloak_bridge import User


class EventGroup(BaseModel):
    name: str


class Event(BaseModel):
    id: str | None = None
    name: str
    start: str | None = None
    event_group: EventGroup


class Institution(BaseModel):
    name: str


class Team(BaseModel):
    id: str | None = None
    name: str
    long_name: str | None = None
    institution: Institution
    team_size: int | None = None


class AwardBadge(BaseModel):
    id: str | None = None
    name: str
    img: str | None = None
    img_dark: str | None = None
    badgr_badge_id: str | None = None


class AwardAssertion(BaseModel):
    id: str | None = None
    badge_id: str
    event_id: str
    team_id: str
    user_id: str
    created: str
    badgr_assertion_id: str | None = None
    badge: AwardBadge
    user: User
    event: Event
    team: Team
