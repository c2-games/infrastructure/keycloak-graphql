# keycloak-graphql

This project provides a graphql interface to keycloak. It is intended for use
as a remote schema with Hasura.

## Setup

### Keycloak authentication

Modify the provided `.env` file to match your environment.

```sh
# Hostname or IP the service should run on
KCGQL_HOST=0.0.0.0
# Port the service should run on
KCGQL_PORT=8000
# Client service account with realm privilege view-users and manage-users
KCGQL_KC_CLIENT_ID=clientclientclient
KCGQL_KC_CLIENT_SECRET=secretsecretsecretsecret
# Keycloak realm
KCGQL_KC_REALM=dev
# Keycloak server base url
KCGQL_KC_SERVER=https://auth.c2games.org
# Keycloak email lifetime when creating accounts or performing actions
KCGQL_KC_EMAIL_LIFETIME=7d
# Enable debugging
KCGQL_DEBUG=true
# Enable or disable the GraphQL Playground interface
KCGQL_PLAYGROUND=true
# Enable or disable graphql schema introspection
KCGQL_INTROSPECTION=true
# Cache lifetime, default is 5 minutes if not set
KCGQL_CACHE_LIFETIME=5m
# How many characters to leave uncensored in the front
# e.g. Billy = B***** or billy@example.com = b*****@example.com
KCGQL_CENSOR_FRONT=1
# How many characters to leave uncensored in the back
# e.g. Billy = *****y or billy@example.com = *****y@example.com
KCGQL_CENSOR_BACK=1
# GraphQL endpoint to check user information such as team association
KCGQL_GQL_URL=http://localhost:8080/v1/graphql
# Issuer Staff User on badgr.com
KCGQL_BADGR_USERNAME="useruseruseruseruseruser"
# Issuer Staff User Password on badgr.com
KCGQL_BADGR_PASSWORD="secretsecretsecretsecretsecret"
# Issuer ID on badgr.com
KCGQL_BADGR_ISSUER_ID="X78X-MX9S6qmwqpqg7LbBw"
# Public API for badgr.io
KCGQL_BADGR_API_ROOT_PUBLIC="https://api.badgr.io/public"
# Authenticated API for badgr.io
KCGQL_BADGR_API_ROOT_AUTHENTICATED="https://api.badgr.io/v2"
# How long to cache Badge Assertion (awards), BadgeClass and Issuer api results
KCGQL_BADGR_CACHE_LIFETIME=2h
```

The `KCGQL_KC_CLIENT_ID` used, should be a keycloak confidential client. The client
needs the `view-users` and `manage-users` role from the `realm-management` client.

The `KCGQL_DEBUG` argument controls the GQL library Debug parameter and will enable
or disable the playground and GQL introspection.

### Python environment

Setup, activate the python virtual environment and install requirements with
the following

```sh
python -m venv venv  # Create a local virtual env directory called venv
. ./venv/bin/activate  # Activate environment
python -m pip install -r requirements.txt  # Install requirements
python -m pip install -r requirements-dev.txt  # Optional developer requirements
```

## Usage

To run this locally simply run `./run.sh`, this will start a local
GraphQL Playground instance at http://localhost:8000/

To run in docker, simply use `docker-compose up`

### Example queries

```gql
query asdf {
  c2games_user_by_pk (id: "6308121b-5c95-4a53-922d-8cf410f0c65b") {
    id
    email
    first_name
    last_name
  }
  bob: c2games_user_by_pk (id: "790973d3-258f-413e-8a7f-57cb8d738fc4") {
    id
    email
    first_name
    last_name
  }
}
```
