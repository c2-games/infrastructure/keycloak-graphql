FROM python:3.10-alpine

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY keycloak_graphql/ keycloak_graphql/

CMD [ "python", "-m", "keycloak_graphql"]