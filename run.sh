#!/bin/bash

THIS_DIR="$(realpath "$(dirname "$0")")"

cd "${THIS_DIR}" || exit

./run_env.sh ./.env "python -m keycloak_graphql"
